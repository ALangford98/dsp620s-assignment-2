import ballerina/io;
import ballerina/kafka;

kafka:ProducerConfiguration producerConfiguration = {

    bootstrapServers: "localhost:9092",
    clientId: "basic-producer",
    acks: "all",
    retryCount: 3,

    valueSerializerType: kafka:SER_STRING,

    keySerializerType: kafka:SER_INT
};

kafka:Producer kafkaProducer = new (producerConfiguration);

public function main() {
    string message = io:readln("\n");
    var sendResult = kafkaProducer->send(message, "VoTo", key = 1);
    if (sendResult is error) {
        io:println("Error occurred while sending data: " + sendResult.toString());
    } else {
        io:println("Message sent successfully.");
    }
    var flushResult = kafkaProducer->flushRecords();
    if (flushResult is error) {
        io:println("Error occurred while flishing the data: " + flushResult.toString());
    } else {
        io:println("Records were flushed successfully.");
    }
}
