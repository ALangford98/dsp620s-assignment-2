import ballerina/kafka;
import ballerina/log;

kafka:ConsumerConfiguration consumerConfigs = {

    bootstrapServers: "localhost:9092",
    groupId: "group-id",

    topics: ["VoTo"],
    pollingIntervalInMillis: 1000,

    keyDeserializerType: kafka:DES_INT,

    valueDeserializerType: kafka:DES_STRING,

    autoCommit: false
};

listener kafka:Consumer consumer = new (consumerConfigs);

service kafkaService on consumer {

    resource function onMessage(kafka:Consumer kafkaConsumer,
            kafka:ConsumerRecord[] records) {

        foreach var kafkaRecord in records {
            processKafkaRecord(kafkaRecord);
        }

        var commitResult = kafkaConsumer->commit();
        if (commitResult is error) {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", commitResult);
        }
    }
}

function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) {
    anydata value = kafkaRecord.value;
    anydata key = kafkaRecord.key;

    if (value is string) {

        if (key is int) {

            log:printInfo("Topic: " + kafkaRecord.topic);
            log:printInfo("Partition: " + kafkaRecord.partition.toString());
            log:printInfo("Key: " + key.toString());
            log:printInfo("Value: " + value);
        } else {
            log:printError("Invalid key type received");
        }
    } else {
        log:printError("Invalid value type received");
    }
}
