import ballerina/http;
import ballerina/docker;
 
@docker:Config {}
service VoTo on new http:Listener(9090){
 
  resource function connect(http:Caller caller,http:Request request) returns error? {
      check caller->respond("Docker Connected!");
  }
}
