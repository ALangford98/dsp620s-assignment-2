import ballerinax/kafka;
import ballerina/graphql;
import ballerina/io;

 

kafka:ProducerConfiguration voting_system = {
	bootstrapServers: "localhost:9091",
	clientId: "register-candidate",
	acks: "all",
	retryCount: 3
	valueSerializerType: kafka:SER_STRING,
	keySerializerType: kafka:SER_INT
};


map<Candidate> registered_candidate_voters ={};
map<Registered_voter> register_vote ={};
map<Vote> accepted_vote ={};
map<rejected_vote> rejected_vote ={};



kafka:Producer voting_systems =checkpanic new (voting_system);


service graphql:Service /graphql on new graphql:Listener(9090) {
 //register the party candidate
    resource function get register_candidate(string name,int id,string party) returns string {
            Candidate candidate ={name,id,party};
            registered_candidate_voters[id.toString()] = {name:name,vID:id,party:party};
            //byte[] serialisedMsg = candidate.toString().toBytes();
             // checkpanic prod->sendProducerRecord({
            //                         topic: "dsp",
            //                         value: serialisedMsg });

            //  checkpanic prod->flushRecords();\
            io:println(registered_candidate_voters);
        return "Candidate registered succesfully : " + name;
    }
    //vote 
     resource function get vote(int voterID,int candidateID) returns string {
            Vote vote_info ={voterID,candidateID};
        //    //check if the details are correct
            if (register_vote.hasKey(voterID.toString()) && registered_candidate_voters.hasKey(candidateID.toString()) ){
                 accepted_vote[id.toString()] = {voterID,candidateID};
                var candidate_name = registered_candidate_voters.[candidateID.toString()]["name"];
               // accepted_vote(vote_info);
    //            int voterID;
    // int candidateID;
    // string rejected_vote;
                //  io:println("very");
               byte[] serialisedMsg = candidate_name.toString().toBytes();

              checkpanic voting_system->sendProducerRecord({
                                    topic: "projectedresults",
                                    value: serialisedMsg });

             checkpanic voting_system->flushRecords();

            }else{
                rejected_vote[id.toString()] = {voterID:voterID,candidateID:candidateID,rejected_vote:rejected_vote};

            }
            // byte[] serialisedMsg = vote_info.toString().toBytes();

            //   checkpanic voting_system->sendProducerRecord({
            //                         topic: "voting",
            //                         value: serialisedMsg });

            //  checkpanic voting_system->flushRecords();
        return "Hello, " ;
    }

// register as a voter
     resource function get register_vote(string name,int National_ID) returns string {
            Registered_voter vote_info ={name,National_ID};
            // Candidate candidate ={name,id,ruling_party};
            register_vote[National_ID.toString()] = {name:name,National_ID:National_ID};

            // byte[] serialisedMsg = vote_info.toString().toBytes();

            //   checkpanic vote_register_prod->sendProducerRecord({
            //                         topic: "dsp",
            //                         value: serialisedMsg });

            //  checkpanic vote_register_prod->flushRecords();
            io:println(register_vote);
        return "voter registered succesfully, " + name;
    }
    //count votes 

}
//records
public type Candidate record {
    string name;
    int id;
    string party;
};
public type Vote record {
    int voterID;
    int candidateID;
    
};
public type Registered_voter record {
    string name;
    int National_ID;
};
public type Regected_vote record {
    int voterID;
    int candidateID;
    string rejected_vote;
};

