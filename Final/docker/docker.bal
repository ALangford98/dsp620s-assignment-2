import ballerina/http;
import ballerina/docker;

@docker:Expose{}
listener http:Listener ListenerEP = new(9090);

@docker:Config {
  name: "voto_system",
  tag:"v1.0"
}

@http:ServiceConfig {
  basePath:"/VoTo"
}
service VoTo on ListenerEP{
  @http:ResourceConfig {
    path:"/action/{input}"
  }
 
  resource function connect(http:Caller caller,http:Request request, string input) returns error? {
      check caller->respond("Docker Connected!\n");
  }
}
